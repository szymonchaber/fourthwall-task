package dev.szymonchaber.fifthwall.image.common.domain.model

data class Image(val id: String, val author: String, val originalUrl: String) {

    val webPUrl = "$originalUrl.webp"

    fun squareUrlForSize(imageSize: Int): String {
        return "${removeOriginalImageSize(originalUrl)}$imageSize.webp"
    }

    private fun removeOriginalImageSize(url: String): String {
        return url.split('/').dropLast(2).joinToString("/", postfix = "/")
    }
}
