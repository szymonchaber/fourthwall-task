package dev.szymonchaber.fifthwall.image.details.presentation

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import dev.szymonchaber.fifthwall.R
import dev.szymonchaber.fifthwall.common.di.CommonComponentHolder
import dev.szymonchaber.fifthwall.image.common.domain.model.Image
import dev.szymonchaber.fifthwall.image.details.di.DaggerImageDetailsComponent
import kotlinx.android.synthetic.main.image_details_fragment.*
import javax.inject.Inject

class ImageDetailsFragment : Fragment(R.layout.image_details_fragment) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ImageDetailsViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        DaggerImageDetailsComponent.builder().commonComponent(CommonComponentHolder.commonComponent)
            .build().inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[ImageDetailsViewModel::class.java]

        observeState()
        val imageId = requireArguments().getString(ARG_IMAGE_ID)
            ?: throw IllegalArgumentException("imageId must not be null")
        viewModel.requestedId = imageId
    }

    private fun observeState() {
        viewModel.stateLiveData.observe(this, Observer {
            onNewState(it)
        })
    }

    private fun onNewState(state: ImageDetailsState) {
        return when (state) {
            is ImageDetailsState.Loading -> showLoading()
            is ImageDetailsState.Error -> showError()
            is ImageDetailsState.ImageInfoLoaded -> showSuccess(state.image)
        }
    }

    private fun showLoading() {
        progressBar.visibility = View.VISIBLE
        errorTextView.visibility = View.GONE
        imageView.visibility = View.GONE
        shareButton.hide()
    }

    private fun showError() {
        progressBar.visibility = View.GONE
        errorTextView.visibility = View.VISIBLE
        imageView.visibility = View.GONE
        shareButton.hide()
        errorTextView.text = getString(R.string.error_occured)
    }

    private fun showSuccess(image: Image) {
        progressBar.visibility = View.GONE
        errorTextView.visibility = View.VISIBLE
        imageView.visibility = View.VISIBLE
        shareButton.show()
        showImageInfo(image)
    }

    private fun showImageInfo(image: Image) {
        Glide.with(this)
            .load(image.webPUrl)
            .centerInside()
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(imageView)
        author.text = image.author
        shareButton.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(
                    Intent.EXTRA_TEXT,
                    getString(R.string.share_image_message, image.originalUrl)
                )
                type = "text/plain"
            }
            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }
    }

    companion object {

        private const val ARG_IMAGE_ID = "IMAGE_ID"

        fun create(imageId: String) = ImageDetailsFragment().apply {
            arguments = Bundle().also {
                it.putString(ARG_IMAGE_ID, imageId)
            }
        }
    }
}
