package dev.szymonchaber.fifthwall.image.list.domain.usecase

import dev.szymonchaber.fifthwall.common.di.FeatureScope
import dev.szymonchaber.fifthwall.image.common.domain.model.Image
import dev.szymonchaber.fifthwall.image.common.domain.repository.ImageRepository
import io.reactivex.Single
import javax.inject.Inject

@FeatureScope
class FetchImageListUseCaseImpl @Inject constructor(private val imageRepository: ImageRepository) :
    FetchImageListUseCase {

    override fun fetchImageList(page: Int, limit: Int): Single<List<Image>> {
        return imageRepository.fetchImageList(page, limit)
    }
}
