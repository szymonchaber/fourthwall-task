package dev.szymonchaber.fifthwall.image.details.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import dev.szymonchaber.fifthwall.R

class ImageDetailsActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.image_details_activity)
        if (savedInstanceState == null) {
            val imageId = intent?.getStringExtra(KEY_IMAGE_ID)
                ?: throw IllegalArgumentException("imageId reguired")
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ImageDetailsFragment.create(imageId))
                .commitNow()
        }
    }

    companion object {

        private const val KEY_IMAGE_ID = "IMAGE_ID"

        fun createIntent(context: Context, imageId: String): Intent {
            return Intent(context, ImageDetailsActivity::class.java)
                .putExtra(KEY_IMAGE_ID, imageId)
        }
    }
}
