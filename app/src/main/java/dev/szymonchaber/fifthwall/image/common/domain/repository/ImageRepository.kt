package dev.szymonchaber.fifthwall.image.common.domain.repository

import dev.szymonchaber.fifthwall.image.common.domain.model.Image
import io.reactivex.Single

interface ImageRepository {

    fun fetchImageList(page: Int, limit: Int): Single<List<Image>>
    fun fetchImageInfo(id: String): Single<Image>
}
