package dev.szymonchaber.fifthwall.image.list.di

import dagger.Component
import dev.szymonchaber.fifthwall.common.di.CommonComponent
import dev.szymonchaber.fifthwall.common.di.FeatureScope
import dev.szymonchaber.fifthwall.common.viewmodel.ViewModelModule
import dev.szymonchaber.fifthwall.image.common.di.ImageModule
import dev.szymonchaber.fifthwall.image.list.presentation.ImageListFragment

@FeatureScope
@Component(
    modules = [ViewModelModule::class, ImageModule::class, ImageListModule::class],
    dependencies = [CommonComponent::class]
)
interface ImageListComponent {

    fun inject(imageListFragment: ImageListFragment)
}
