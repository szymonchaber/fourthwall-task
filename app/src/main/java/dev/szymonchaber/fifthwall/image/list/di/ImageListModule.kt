package dev.szymonchaber.fifthwall.image.list.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.szymonchaber.fifthwall.common.di.FeatureScope
import dev.szymonchaber.fifthwall.common.viewmodel.ViewModelKey
import dev.szymonchaber.fifthwall.image.list.domain.usecase.FetchImageListUseCase
import dev.szymonchaber.fifthwall.image.list.domain.usecase.FetchImageListUseCaseImpl
import dev.szymonchaber.fifthwall.image.list.presentation.ImageListViewModel

@Module
abstract class ImageListModule {

    @FeatureScope
    @Binds
    @IntoMap
    @ViewModelKey(ImageListViewModel::class)
    internal abstract fun bindImageListViewModel(viewModel: ImageListViewModel): ViewModel

    @FeatureScope
    @Binds
    abstract fun bindFetchImageListUseCase(fetchImageListUseCaseImpl: FetchImageListUseCaseImpl):
            FetchImageListUseCase
}
