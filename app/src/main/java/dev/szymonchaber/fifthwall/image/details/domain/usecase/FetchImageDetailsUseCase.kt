package dev.szymonchaber.fifthwall.image.details.domain.usecase

import dev.szymonchaber.fifthwall.image.common.domain.model.Image
import io.reactivex.Single

interface FetchImageDetailsUseCase {

    fun fetchImageInfo(id: String): Single<Image>
}
