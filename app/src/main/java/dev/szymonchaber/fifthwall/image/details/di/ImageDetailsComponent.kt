package dev.szymonchaber.fifthwall.image.details.di

import dagger.Component
import dev.szymonchaber.fifthwall.common.di.CommonComponent
import dev.szymonchaber.fifthwall.common.di.FeatureScope
import dev.szymonchaber.fifthwall.common.viewmodel.ViewModelModule
import dev.szymonchaber.fifthwall.image.common.di.ImageModule
import dev.szymonchaber.fifthwall.image.details.presentation.ImageDetailsFragment

@FeatureScope
@Component(
    modules = [ViewModelModule::class, ImageModule::class, ImageDetailsModule::class],
    dependencies = [CommonComponent::class]
)
interface ImageDetailsComponent {

    fun inject(imageDetailsFragment: ImageDetailsFragment)
}
