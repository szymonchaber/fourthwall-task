package dev.szymonchaber.fifthwall.image.details.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.szymonchaber.fifthwall.common.di.FeatureScope
import dev.szymonchaber.fifthwall.image.details.domain.usecase.FetchImageDetailsUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import timber.log.Timber
import javax.inject.Inject

@FeatureScope
class ImageDetailsViewModel @Inject constructor(private val fetchImageDetailsUseCase: FetchImageDetailsUseCase) :
    ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val _stateLiveData = MutableLiveData<ImageDetailsState>()
    val stateLiveData: LiveData<ImageDetailsState>
        get() = _stateLiveData

    var requestedId: String? = null
        set(value) {
            field = value
            loadImageDetails()
        }

    private fun loadImageDetails() {
        requestedId?.let {
            if (stateLiveData.value != ImageDetailsState.Loading) {
                fetchDetails(it)
            }
        }
    }

    private fun fetchDetails(requestedId: String) {
        _stateLiveData.postValue(ImageDetailsState.Loading)
        fetchImageDetailsUseCase.fetchImageInfo(requestedId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _stateLiveData.postValue(ImageDetailsState.ImageInfoLoaded(it))
            }, {
                Timber.e(it)
                _stateLiveData.postValue(ImageDetailsState.Error(it))
            }).addTo(compositeDisposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}
