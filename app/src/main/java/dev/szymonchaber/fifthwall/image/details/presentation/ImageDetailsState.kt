package dev.szymonchaber.fifthwall.image.details.presentation

import dev.szymonchaber.fifthwall.image.common.domain.model.Image

sealed class ImageDetailsState {

    object Loading : ImageDetailsState()
    class ImageInfoLoaded(val image: Image) : ImageDetailsState()
    class Error(val throwable: Throwable) : ImageDetailsState()
}
