package dev.szymonchaber.fifthwall.image.list.presentation

import android.graphics.Point
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import dev.szymonchaber.fifthwall.R
import dev.szymonchaber.fifthwall.common.di.CommonComponentHolder
import dev.szymonchaber.fifthwall.image.common.domain.model.Image
import dev.szymonchaber.fifthwall.image.details.presentation.ImageDetailsActivity
import dev.szymonchaber.fifthwall.image.list.di.DaggerImageListComponent
import kotlinx.android.synthetic.main.main_fragment.*
import javax.inject.Inject

class ImageListFragment : Fragment(R.layout.main_fragment) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ImageListViewModel

    private lateinit var imageListAdapter: ImageListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        DaggerImageListComponent.builder().commonComponent(CommonComponentHolder.commonComponent)
            .build().inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[ImageListViewModel::class.java]
        setupViews()
        observeState()
    }

    private fun setupViews() {
        recyclerView.layoutManager = GridLayoutManager(requireContext(), getSpanSize())
        imageListAdapter = ImageListAdapter(requireContext(), getImageWidth()) {
            showImageDetails(it)
        }
        recyclerView.adapter = imageListAdapter
    }

    private fun showImageDetails(image: Image) {
        startActivity(ImageDetailsActivity.createIntent(requireContext(), image.id))
    }

    private fun getImageWidth() = getScreenSize() / getSpanSize()

    private fun getScreenSize(): Int {
        val display = requireActivity().windowManager.defaultDisplay
        val size = Point().apply {
            display.getSize(this)
        }
        return size.x
    }

    private fun getSpanSize() = resources.getInteger(R.integer.span_size)

    private fun observeState() {
        viewModel.stateLiveData.observe(this, Observer {
            onNewState(it)
        })
    }

    private fun onNewState(state: ImageListState) {
        return when (state) {
            is ImageListState.Loading -> showLoading()
            is ImageListState.Error -> showError()
            is ImageListState.ImagesLoaded -> showSuccess(state.images)
        }
    }

    private fun showLoading() {
        progressBar.visibility = View.VISIBLE
        errorTextView.visibility = View.GONE
        recyclerView.visibility = View.GONE
    }

    private fun showError() {
        progressBar.visibility = View.GONE
        errorTextView.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
        errorTextView.text = getString(R.string.error_occured)
    }

    private fun showSuccess(images: List<Image>) {
        progressBar.visibility = View.GONE
        errorTextView.visibility = View.VISIBLE
        recyclerView.visibility = View.VISIBLE
        imageListAdapter.newData(images)
    }

    companion object {

        fun create() = ImageListFragment()
    }
}
