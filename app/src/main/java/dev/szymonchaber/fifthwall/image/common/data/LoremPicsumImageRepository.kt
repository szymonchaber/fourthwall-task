package dev.szymonchaber.fifthwall.image.common.data

import dev.szymonchaber.fifthwall.common.di.FeatureScope
import dev.szymonchaber.fifthwall.image.common.data.api.LoremPicsumService
import dev.szymonchaber.fifthwall.image.common.data.api.model.PicsumImage
import dev.szymonchaber.fifthwall.image.common.domain.model.Image
import dev.szymonchaber.fifthwall.image.common.domain.repository.ImageRepository
import io.reactivex.Single
import javax.inject.Inject

@FeatureScope
class LoremPicsumImageRepository @Inject constructor(private val loremPicsumService: LoremPicsumService) :
    ImageRepository {

    override fun fetchImageList(page: Int, limit: Int): Single<List<Image>> {
        return loremPicsumService.getImagesList(page, limit).map {
            it.map(this::toDomainImage)
        }
    }

    override fun fetchImageInfo(id: String): Single<Image> {
        return loremPicsumService.getImageInfo(id).map(this::toDomainImage)
    }

    private fun toDomainImage(picsumImage: PicsumImage): Image {
        return Image(picsumImage.id, picsumImage.author, picsumImage.downloadUrl)
    }
}
