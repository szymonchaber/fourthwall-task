package dev.szymonchaber.fifthwall.image.list.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import dev.szymonchaber.fifthwall.R
import dev.szymonchaber.fifthwall.image.common.domain.model.Image
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_image.*

class ImageListAdapter(
    context: Context,
    private val imageWidth: Int,
    imageClickListener: (Image) -> Unit
) :
    RecyclerView.Adapter<ImageListAdapter.ImageViewHolder>() {

    private val layoutInflater = LayoutInflater.from(context)
    private val glide = Glide.with(context)

    private val images: MutableList<Image> = mutableListOf()

    private val clickListener: (Int) -> Unit = { position ->
        imageClickListener(images[position])
    }

    fun newData(newImages: List<Image>) {
        images.clear()
        images.addAll(newImages)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            layoutInflater.inflate(R.layout.item_image, parent, false),
            glide,
            clickListener
        )
    }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(images[position], imageWidth)
    }

    class ImageViewHolder(
        view: View,
        private val glide: RequestManager,
        onClickListener: (Int) -> Unit
    ) :
        RecyclerView.ViewHolder(view), LayoutContainer {

        override val containerView: View?
            get() = itemView

        init {
            imageView.setOnClickListener {
                onClickListener(adapterPosition)
            }
        }

        fun bind(
            image: Image,
            imageSize: Int
        ) {
            val squareUrlForSize = image.squareUrlForSize(imageSize)
            glide
                .load(squareUrlForSize)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(imageView)
        }
    }
}
