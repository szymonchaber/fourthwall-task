package dev.szymonchaber.fifthwall.image.details.domain.usecase

import dev.szymonchaber.fifthwall.image.common.domain.model.Image
import dev.szymonchaber.fifthwall.image.common.domain.repository.ImageRepository
import io.reactivex.Single
import javax.inject.Inject

class FetchImageDetailsUseCaseImpl @Inject constructor(private val repository: ImageRepository) :
    FetchImageDetailsUseCase {

    override fun fetchImageInfo(id: String): Single<Image> {
        return repository.fetchImageInfo(id)
    }
}
