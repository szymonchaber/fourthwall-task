package dev.szymonchaber.fifthwall.image.common.data.api

import dev.szymonchaber.fifthwall.image.common.data.api.model.PicsumImage
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface LoremPicsumService {

    @GET("v2/list")
    fun getImagesList(@Query("page") page: Int, @Query("limit") limit: Int): Single<List<PicsumImage>>

    @GET("id/{id}/info")
    fun getImageInfo(@Path("id") id: String): Single<PicsumImage>
}
