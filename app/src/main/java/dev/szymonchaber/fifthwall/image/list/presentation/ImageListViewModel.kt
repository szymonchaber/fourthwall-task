package dev.szymonchaber.fifthwall.image.list.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.szymonchaber.fifthwall.common.di.FeatureScope
import dev.szymonchaber.fifthwall.image.list.domain.usecase.FetchImageListUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import timber.log.Timber
import javax.inject.Inject

@FeatureScope
class ImageListViewModel @Inject constructor(private val fetchImageListUseCase: FetchImageListUseCase) :
    ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val _stateLiveData = MutableLiveData<ImageListState>()
    val stateLiveData: LiveData<ImageListState>
        get() = _stateLiveData

    init {
        loadImageList()
    }

    private fun loadImageList() {
        if (stateLiveData.value != ImageListState.Loading) {
            fetchImages()
        }
    }

    private fun fetchImages() {
        _stateLiveData.postValue(ImageListState.Loading)
        fetchImageListUseCase.fetchImageList(0, 100)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _stateLiveData.postValue(ImageListState.ImagesLoaded(it))
            }, {
                Timber.e(it)
                _stateLiveData.postValue(ImageListState.Error(it))
            }).addTo(compositeDisposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}
