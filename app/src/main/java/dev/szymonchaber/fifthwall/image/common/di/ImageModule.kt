package dev.szymonchaber.fifthwall.image.common.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import dev.szymonchaber.fifthwall.common.di.FeatureScope
import dev.szymonchaber.fifthwall.image.common.data.LoremPicsumImageRepository
import dev.szymonchaber.fifthwall.image.common.data.api.LoremPicsumService
import dev.szymonchaber.fifthwall.image.common.domain.repository.ImageRepository
import retrofit2.Retrofit

@Module
abstract class ImageModule {

    @FeatureScope
    @Binds
    abstract fun bindImageRepository(loremPicsumImageRepository: LoremPicsumImageRepository): ImageRepository

    @Module
    companion object {

        @FeatureScope
        @Provides
        @JvmStatic
        fun provideLoremPicsumService(retrofit: Retrofit): LoremPicsumService {
            return retrofit.create(LoremPicsumService::class.java)
        }
    }
}
