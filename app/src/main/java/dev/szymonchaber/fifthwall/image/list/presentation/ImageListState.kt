package dev.szymonchaber.fifthwall.image.list.presentation

import dev.szymonchaber.fifthwall.image.common.domain.model.Image

sealed class ImageListState {

    object Loading : ImageListState()
    class ImagesLoaded(val images: List<Image>) : ImageListState()
    class Error(val throwable: Throwable) : ImageListState()
}
