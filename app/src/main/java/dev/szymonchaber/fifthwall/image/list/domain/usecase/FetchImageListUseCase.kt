package dev.szymonchaber.fifthwall.image.list.domain.usecase

import dev.szymonchaber.fifthwall.image.common.domain.model.Image
import io.reactivex.Single

interface FetchImageListUseCase {

    fun fetchImageList(page: Int, limit: Int): Single<List<Image>>
}
