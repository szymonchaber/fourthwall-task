package dev.szymonchaber.fifthwall.image.details.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.szymonchaber.fifthwall.common.di.FeatureScope
import dev.szymonchaber.fifthwall.common.viewmodel.ViewModelKey
import dev.szymonchaber.fifthwall.image.details.domain.usecase.FetchImageDetailsUseCase
import dev.szymonchaber.fifthwall.image.details.domain.usecase.FetchImageDetailsUseCaseImpl
import dev.szymonchaber.fifthwall.image.details.presentation.ImageDetailsViewModel

@Module
abstract class ImageDetailsModule {

    @FeatureScope
    @Binds
    @IntoMap
    @ViewModelKey(ImageDetailsViewModel::class)
    internal abstract fun bindImageDetailsViewModel(viewModel: ImageDetailsViewModel): ViewModel

    @FeatureScope
    @Binds
    abstract fun bindFetchImageDetailsUseCase(
        fetchImageDetailsUseCaseImpl: FetchImageDetailsUseCaseImpl
    ): FetchImageDetailsUseCase
}
