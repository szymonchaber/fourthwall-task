package dev.szymonchaber.fifthwall.common.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        IOModule::class
    ]
)
interface CommonComponent {

    val retrofit: Retrofit

    @Component.Factory
    interface Factory {

        fun create(@BindsInstance applicationContext: Context): CommonComponent
    }
}
