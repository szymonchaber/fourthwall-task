package dev.szymonchaber.fifthwall.common.di

import android.content.Context

object CommonComponentHolder {

    lateinit var commonComponent: CommonComponent
        private set

    fun init(context: Context) {
        commonComponent = DaggerCommonComponent.factory().create(context.applicationContext)
    }
}
