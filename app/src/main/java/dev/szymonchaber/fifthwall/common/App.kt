package dev.szymonchaber.fifthwall.common

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import dev.szymonchaber.fifthwall.BuildConfig
import dev.szymonchaber.fifthwall.common.di.CommonComponentHolder
import timber.log.Timber
import timber.log.Timber.DebugTree


@Suppress("unused")
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant()
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
        AndroidThreeTen.init(this)
        CommonComponentHolder.init(this)
    }
}
