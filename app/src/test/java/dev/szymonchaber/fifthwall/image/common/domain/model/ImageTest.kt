package dev.szymonchaber.fifthwall.image.common.domain.model

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class ImageTest {

    @Test
    fun `should return correctly resized url in WebP format`() {
        // given
        val url = "https://picsum.photos/id/1000/5626/3635"
        val image = Image("", "", url)
        val expectedWidth = 500

        // when
        val newUrl = image.squareUrlForSize(expectedWidth)

        // then
        assertThat(newUrl).isEqualTo("https://picsum.photos/id/1000/500.webp")
    }

    @Test
    fun `should return original url in WebP format`() {
        // given
        val url = "https://picsum.photos/id/1000/5626/3635"
        val image = Image("", "", url)

        // when
        val newUrl = image.webPUrl

        // then
        assertThat(newUrl).isEqualTo("https://picsum.photos/id/1000/5626/3635.webp")
    }
}
