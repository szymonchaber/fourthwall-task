# Fifthwall

### Compromises
I decided not to focus on paging, as I wanted to showcase my consideration for things like
architecture, image download size etc.
 
In general, I would split the project into separate modules, but didn't do it to focus on other sides of the project.
However, splitting the project would be easy because I already follow Modular Monolith architecture, 
along with Clean Architecture.

I included one example of unit tests I typically write.
